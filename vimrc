" To edit this file use `:e $MYVIMRC` command
" To apply settings while editing use `:so %`
" Install neobundle on *nix `git clone https://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim`
" To install neobundle on win run at home dir `git clone https://github.com/Shougo/neobundle.vim vimfiles/bundle/neobundle.vim`
" Credits (configs I got inspiration from):
" https://github.com/klen/.vim/
" https://github.com/joedicastro/dotfiles/tree/master/vim/
" https://github.com/L0stSoul/vim-config/
" https://github.com/spf13/spf13-vim/
" https://github.com/bling/dotvim/
" ОС-зависимые настройки
if has('win32') || has('win64')
	let $CONFDIR = $HOME.'/vimfiles'
	" Статья про шаманство с кодировками под win7 http://habrahabr.ru/post/183222/
	" Отображение кириллицы во внутренних сообщениях программы
	lan mes ru_RU.UTF-8
	" Отображение кириллицы в меню
	source $VIMRUNTIME/delmenu.vim
	set langmenu=ru_RU.UTF-8
	source $VIMRUNTIME/menu.vim
else
	let $CONFDIR = $HOME.'/.vim'
endif


" Setup
" =====
if has('vim_starting')
	" вырубаем режим совместимости с VI
	" явное лучше скрытого http://habrahabr.ru/post/196550/#comment_6831880
	set nocompatible
	let g:SESSION_DIR=$CONFDIR.'/sessions'

	" Create sessions/backup dirs
	if finddir(g:SESSION_DIR) == ''
		silent call mkdir(g:SESSION_DIR, "p")
	endif
	" Setup for neobundle
	" -------------------
	" Required for neobundle:
	set runtimepath+=$CONFDIR/bundle/neobundle.vim/
endif

	" Next two lines must be before color bundle in order it to work.
	filetype plugin indent on


" Settings
" ========
let mapleader="," " setting the leader key
let maplocalleader = " "
if has('gui_running')
	" Changing font
	if has('win32') || has('win64')
		set guifont=Consolas:h11:cRUSSIAN::
	else
		set guifont=Terminus\ 12
	endif
	" Maximizing editor. Not a full solution, since full solutions for
	" Windows are too nerdy even for me. http://habrahabr.ru/post/29410/
	set lines=999 columns=999
	set guioptions-=T " Do not want toolbar
endif

if has('clipboard')
	if has('unnamedplus') " When possible use + register for copy-paste
		set clipboard=unnamedplus
	else " On mac and Windows, use * register for copy-paste
		set clipboard=unnamed
	endif
endif

" Learn vim script the hard way exercises. Please, skip it.
:echo '(>^.^<) - lya kotya nyau!!!'
:noremap <leader>_ ddp

set hidden   " Edit multiple unsaved files at the same time
set confirm  " Prompt to save unsaved changes when exiting
set autoread " auto reload changed files
" Display options
set title " show file name in window title
set ttyfast " better screen redraw (esp. in windows gvim) (Indicates fast terminal connection)
set novisualbell "Не мигать
set wrap " Переносим на другую строчку
set linebreak " разрываем строки
set winminheight=0 " minimal window height
set winminwidth=0 " minimal window width
set scrolloff=4 " min 4 symbols below cursor
" Numbers of columns to scroll when the cursor get off the screen
" Useless with sidescrolloff
" set sidescroll=4
set sidescrolloff=10 " Numbers of columns to keep to the left and to the right off the screen
set showcmd " отображение выполняемой команды
set whichwrap+=<,>,[,]
" ... some klen options skipped
" Enable Tcl interface. Not sure what is exactly meant.
" set infercase
set laststatus=2 " Always show a statusline
" Don't try to highlight lines longer than 800 characters.
set synmaxcol=800
" The cursor should stay where you leave it, instead of moving to the first non
" blank of the line
set nostartofline

" Matching characters
set showmatch " Show matching brackets
set matchpairs+=<:> " Make < and > match as well

" Quickly time out on keycodes, but never time out on mappings
set notimeout ttimeout ttimeoutlen=200
" Включаем 256 цветов в терминале
set t_Co=256

" настройки табов для python
set autoindent " Copy indent from the previous line
set smartindent  " enable nice indent
set noexpandtab    " tab with spaces
set smarttab     " indent using shiftwidth
set tabstop=4    " length of the unexpanded tab character
set shiftwidth=4 " spaces count in one indent
set softtabstop=4 " tab like 4 spaces
set shiftround    " align indent according to shiftwidth

" Поиск
set gdefault " Add the g flag to search/replace by default
set hlsearch   " подсветка результатов поиска
set ignorecase " игнорировать регистр при поиске
set smartcase  " вкупе с ignorecase работает так: если только нижний регистр, поиск без учета регистра, если же есть и верхний, то с учетом
set incsearch  " инкрементный поиск (по мере ввода)

" Localization
" ------------
"
"  This block must locate *before* the listchars setting in case the latter
"  contains non-trivial characters.
set keymap=russian-jcukenwin
set iminsert=0
set imsearch=-1
set spelllang=en,ru
set encoding=utf-8 "Кодировка файлов по умолчанию
set fileencodings=utf-8,cp1251,koi8-r,cp866
set termencoding=utf-8 "Кодировка терминала

" Отобразим непечатаемые символы
if has('multi_byte')
	set listchars=tab:»\ ,trail:·,eol:¶,extends:→,precedes:←,nbsp:×
	" it is an unstoppable inventiveness of people, here is my collection:
	" set listchars=tab:⇥\ ,trail:·,extends:⋯,precedes:⋯,eol:$,nbsp:~ " klen
	" set listchars=tab:→\ ,eol:↵,trail:·,extends:↷,precedes:↶ " joedicastro
endif
set nolist " Hide invisible characters by default


" Подсвечиваем все, что можно подсвечивать
let python_highlight_all=1

" Автокомплит по табу
function! InsertTabWrapper()
let col=col('.') - 1
if !col || getline('.')[col - 1 ] !~ '\k'
return "\"
else
return "\<c-p>"
endif
endfunction
imap <c-r>=InsertTabWrapper()" Показываем все полезные опции автокомплита сразу
set complete=""
set complete+=.
set complete+=k
set complete+=b
set complete+=t

" Перед сохранением вырезаем пробелы на концах (только в .py файлах)
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
" В .py файлах включаем умные отступы после ключевых слов
autocmd BufRead *.py set smartindent
set cinwords=if,elif,else,for,while,try,except,finally,def,class

" выключить подсветку: повесить на горячую клавишу Ctrl-F8
nnoremap <C-F8> :nohlsearch<CR>

""" Прочие настройки
"set nu            " Включить нумерацию строк
set numberwidth=1 " Keep line numbers small if it's shown

set mousehide " Спрятать курсор мыши, когда набираем текст
set mouse=a   " Включить поддержку мыши

" Удобное поведение backspace
set backspace=indent,eol,start

" Tab completion in command line mode
set wildmenu                   " Better commandline completion
set wildmode=longest:full,full " Expand match on first Tab complete
set wildcharm=<TAB> " Autocmpletion hotkey
set wildignore+=.hg,.git,.svn                    " Version control
set wildignore+=*.aux,*.out,*.toc                " LaTeX intermediate files
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg   " binary images
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest " compiled object files
set wildignore+=*.spl                            " compiled spelling word lists
set wildignore+=*.sw?                            " Vim swap files
set wildignore+=*.DS_Store                       " OSX bullshit
set wildignore+=*.luac                           " Lua byte code
set wildignore+=migrations                       " Django migrations
set wildignore+=*.pyc                            " Python byte code
set wildignore+=*.orig                           " Merge resolution files



" Колоночка, чтоб показывать плюсики для сворачивания кода
set foldcolumn=1
set foldlevelstart=99 " open all folds by default
if v:version >= 703
	" Textwidth projection into interface
	set colorcolumn=+1
	highlight ColorColumn ctermbg=darkGrey
endif

" Вырубаем .swp и ~ (резервные файлы)
set nobackup
set noswapfile
" Disable vim common sequense for saving.
" By defalut vim write buffer to a new file, then delete original file
" then rename the new file.
set nowritebackup
" Enable extended matchit
runtime macros/matchit.vim

""" Работа с вкладками
fu! TabMoveLeft()
	let current_tab = tabpagenr()
	if current_tab > 1
		let current_tab = current_tab - 2
		execute 'tabmove' current_tab
	endif
endf

fu! TabMoveRight()
	let current_tab = tabpagenr()
	execute 'tabmove' current_tab
endf

" предыдущая вкладка
nmap <silent><A-j> :tabprevious<CR>
imap <silent><A-j> <C-O>:tabprevious<CR>
vmap <silent><A-j> <ESC>:tabprevious<CR>

" следующая вкладка
nmap <silent><A-k> :tabnext<CR>
imap <silent><A-k> <C-O>:tabnext<CR>
vmap <silent><A-k> <ESC>:tabnext<CR>

" первая вкладка
nmap <silent><A-h> :tabfirst<CR>
imap <silent><A-h> <C-O>:tabfirst<CR>
vmap <silent><A-h> <ESC>:tabfirst<CR>

" последняя вкладка
nmap <silent><A-l> :tablast<CR>
imap <silent><A-l> <C-O>:tablast<CR>
vmap <silent><A-l> <ESC>:tablast<CR>


" Autocommands
" ============

if has("autocmd")
	augroup vimrc
	au!
		" Autosave last session
		if has('mksession')
			au VimLeavePre * exe "mks! " g:SESSION_DIR.'/last.vim'
		endif
		" Auto reload vim after you change it
        au BufWritePost *.vim source $MYVIMRC | AirlineRefresh
        au BufWritePost .vimrc source $MYVIMRC | AirlineRefresh
        " Windows variant
        au BufWritePost vimrc source $MYVIMRC | AirlineRefresh
		" Настройка omnicompletion для python
		" commented based on this: http://habrahabr.ru/post/173473/#comment_6027257
		" autocmd FileType python set omnifunc=pythoncomplete#Complete
		autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
		autocmd FileType html,htmldjango set omnifunc=htmlcomplete#CompleteTags
		autocmd FileType css,scss set omnifunc=csscomplete#CompleteCSS
		" Setting indentation/textwidth/wordwrap by filetype
		" softtabstop tabstop shiftwidth expandtab textwidth formatoptions
		" fo 1 note: it's awful, since it considers single signs (plus,
		"     hyphen-minus, etc.) as one-letter words
		autocmd FileType rst    setl sts=3 ts=6 sw=3 et textwidth=78 fo+=tcrqnl
		autocmd FileType python setl sts=4 ts=4 sw=4 et textwidth=80 fo-=tca
	augroup END
endif


" Keymappings
" ===========

" Normal mode {{{
" -----------

" Small fixes I tired to type {{{
" <leader>f - the mnemonic is 'fix'
" Fix django templates
nnoremap <leader>fdt :set filetype=htmldjango<cr>
" Fix skype (windows) line-endings (doesn't work =( )
nnoremap <leader>fle :%s///g<cr>
" <leader>y - the mnemonic is 'yank'
" Not a fix, but I tired to recall and type this too
" Yank All
nnoremap <leader>ya :%y *<cr>
" }}}

" Reformat current paragraph
nnoremap Q gqap

" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
nnoremap Y y$

" Navigation
nnoremap j gj
nnoremap k gk
nnoremap gj j
nnoremap gk k

" Navigation in quickfix and location lists
nnoremap <left> :cprev<cr>zvzz
nnoremap <right> :cnext<cr>zvzz
nnoremap <up> :lprev<cr>zvzz
nnoremap <down> :lnext<cr>zvzz

" <leader>p - the mnemonic is 'paste'
" Toggle paste mode
noremap <leader>p :set paste!<CR>
" An alternative way of the same setting in case I decide to apply it
" set pastetoggle=<leader>p

" Not jump on star, only highlight
nnoremap * *N
" Drop hightlight search result
noremap <leader><space> :set hlsearch!<CR>

" Buffer commands
" <leader>w - the mnemonic is 'write'
noremap <silent> <leader>ww :up<CR>
" FIXME: redundant due to <leader>K
" <leader>b - the mnemonic is 'buffer'
noremap <silent> <leader>bd :bd<CR>
noremap <silent> <leader>bn :bn<CR>
noremap <silent> <leader>bp :bp<CR>
" <leader>q - the mnemonic is 'quit'
" Close files
nnoremap <silent> <leader>qq :q<CR>

" Switch options

" <leader>o - the mnemonic is 'options'
" Toggle invisible chars
nnoremap <silent> <leader>ol :set list! list?<CR>
" Toggle line wraps
nnoremap <silent> <leader>ow :set wrap! wrap?<CR>
nnoremap <silent> <leader>on :call ToggleRelativeAbsoluteNumber()<CR>
function! ToggleRelativeAbsoluteNumber()
	if !&number && !&relativenumber
		set number
		set relativenumber
	elseif &relativenumber
		set norelativenumber
		set number
	elseif &number
		set nonumber
	endif
endfunction


" New windows
" <leader>s - the mnemonic is 'split'
nnoremap <leader>sv <C-w>v
nnoremap <leader>sh <C-w>s

" <leader>e - the mnemonic is 'edit'
" edit vimrc
nnoremap <leader>ev :split $MYVIMRC<cr>

" <leader>k - the mnemonic is 'kill'
" Fast window & buffer close and kill
nnoremap <leader>k <C-w>c
nnoremap <silent><Leader>K :bd<CR>

" Quick exiting without save
nnoremap <leader>`` :qa!<CR>

"закрыть вкладку по CTRL-F4
nnoremap <C-F4> :tabclose<CR>
" auto center {{{
    nnoremap <silent> n nzz
    nnoremap <silent> N Nzz
    nnoremap <silent> * *zz
    nnoremap <silent> # #zz
    nnoremap <silent> g* g*zz
    nnoremap <silent> g# g#zz
    nnoremap <silent> <C-o> <C-o>zz
    nnoremap <silent> <C-i> <C-i>zz
" }}}
" }}}

" Visual mode {{{
" -----------
" Visual shifting (does not exit Visual mode)
vnoremap < <gv
vnoremap > >gv
" }}}

" Insert mode {{{
" -----------
:inoremap jk <esc>
" }}}

" переместить вкладку в начало
nmap <silent><A-S-h> :tabmove 0<CR>
imap <silent><A-S-h> <C-O>:tabmove 0<CR>
vmap <silent><A-S-h> <ESC>:tabmove 0<CR>

" переместить вкладку в конец
nmap <silent><A-S-l> :tabmove<CR>
imap <silent><A-S-l> <C-O>:tabmove<CR>
vmap <silent><A-S-l> <ESC>:tabmove<CR>

" переместить вкладку назад
nmap <silent><A-S-j> :call TabMoveLeft()<CR>
imap <silent><A-S-j> <C-O>:call TabMoveLeft()<CR>
vmap <silent><A-S-j> <ESC>:call TabMoveLeft()<CR>

" переместить вкладку вперёд
nmap <silent><A-S-k> :call TabMoveRight()<CR>
imap <silent><A-S-k> <C-O>:call TabMoveRight()<CR>
vmap <silent><A-S-k> <ESC>:call TabMoveRight()<CR>

" аналогичное перемещение по "окнам"
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" Things to unlearn
inoremap <C-[> <nop>


" Bundles (plugins)
" =================

" Required for neobundle:
" This call must be out of startup section. See neobundle faq for the details.
" This is the new neobundle interface itroduced at
" https://github.com/Shougo/neobundle.vim/commit/f9c566efbd117d7189ea4c951e3523b33f15b453
" instead of neobundle#rc
call neobundle#begin(expand($CONFDIR.'/bundle/'))

" Neobundle itself
" ----------------
NeoBundleFetch 'Shougo/neobundle.vim'

" Version control {{{
" ---------------
" fugitive for hg
NeoBundle "bitbucket:ludovicchabant/vim-lawrencium"
" <leader>h - the mnemonic is 'hg'
nnoremap <leader>hc :Hgcommit<CR>

" a Git wrapper so awesome, it should be illegal
" I don't like git, but millions of lemmings cannot be wrong choosing github
NeoBundle 'tpope/vim-fugitive'
cabbrev git Git
" <leader>g - the mnemonic is 'git'
" git diff
nmap <silent> <leader>gd :Gdiff<cr>
" git commit
nnoremap <leader>gc :Gcommit -a<CR>
nnoremap <leader>gl :Git log -3<CR>
nnoremap <leader>gpl :Git pull origin master<CR>
nnoremap <leader>gpm :Git push origin master<CR>
nnoremap <leader>gpp :Git push<CR>
" nmap <silent> <leader>gc :Gcommit<cr>

NeoBundle 'mhinz/vim-signify' "{{{
	let g:signify_update_on_bufenter=0
    let g:signify_vcs_list = [ 'hg', 'git' ]
"}}}
" }}}

" Interactive command execution in Vim
" **A requirement** for some Unite features
NeoBundle 'Shougo/vimproc', {
	\ 'build' : {
	\     'windows' : 'make -f make_mingw32.mak',
	\     'cygwin' : 'make -f make_cygwin.mak',
	\     'mac' : 'make -f make_mac.mak',
	\     'unix' : 'make -f make_unix.mak',
	\    },
	\ }

" Extends 'mattn/emmet-vim' with custom snippets
NeoBundle 'mattn/webapi-vim'

" Abbreviations expander. What is the difference to snippets?
NeoBundleLazy 'mattn/emmet-vim' , {'autoload': {'filetypes': ['html', 'xhtml', 'css', 'xml', 'xls', 'markdown']}}

" Snippets {{{
" --------
" I'd like to start from neocomplcache + neosnippet
" Code complete {{{
" I am a little confused about completion plugins...
" NeoBundle 'davidhalter/jedi-vim'
" let g:jedi#popup_select_first = 0 " Disable first select from auto-complete

NeoBundle 'Shougo/neocomplete.vim'
" Enable NeoComplete at startup
let g:neocomplete#enable_at_startup = 1
" Max items in code-complete
let g:neocomplete#max_list = 10
" Max width of code-complete window
let g:neocomplete#max_keyword_width = 80
" Code complete is ignoring case until no Uppercase letter is in input
let g:neocomplete#enable_smart_case = 1
" Auto select first item in code-complete
let g:neocomplete#enable_auto_select = 1
" Disable auto popup
let g:neocomplete#disable_auto_complete = 1

" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"

" NeoBundle 'Shougo/neocomplcache.vim'

" " Enable NeocomplCache at startup
" let g:neocomplcache_enable_at_startup = 1
" " Max items in code-complete
" let g:neocomplcache_max_list = 10
" " Max width of code-complete window
" let g:neocomplcache_max_keyword_width = 80
" " Code complete is ignoring case until no Uppercase letter is in input
" let g:neocomplcache_enable_smart_case = 1
" " Auto select first item in code-complete
" let g:neocomplcache_enable_auto_select = 1
" " Disable auto popup
" let g:neocomplcache_disable_auto_complete = 1

"  Well, I cannot recall where from did I get this function.
" " Smart tab Behavior
" function! CleverTab()
    " " If autocomplete window visible then select next item in there
    " if pumvisible()
        " return "\<C-n>"
    " endif
    " " If it's begining of the string then return just tab pressed
    " let substr = strpart(getline('.'), 0, col('.') - 1)
    " let substr = matchstr(substr, '[^ \t]*$')
    " if strlen(substr) == 0
        " " nothing to match on empty string
        " return "\<Tab>"
    " else
        " " If not begining of the string, and autocomplete popup is not visible
        " " Open this popup
        " return "\<C-x>\<C-u>"
    " endif
" endfunction
" inoremap <expr><TAB> CleverTab()
" " Undo autocomplete
" inoremap <expr><C-e> neocomplcache#undo_completion()
""" }}}

" Snippets engine with good integration with neocomplcache
NeoBundle 'Shougo/neosnippet'
" Default snippets for neosnippet, i prefer vim-snippets
"NeoBundle 'Shougo/neosnippet-snippets'
" Default snippets
NeoBundle 'honza/vim-snippets'

" Enable snipMate compatibility
let g:neosnippet#enable_snipmate_compatibility = 1
" Tell Neosnippet about the other snippets
let g:neosnippet#snippets_directory='~/.vim/bundle/vim-snippets/snippets'
" Disables standart snippets. We use vim-snippets bundle instead
let g:neosnippet#disable_runtime_snippets = { '_' : 1 }
" Expand snippet and jimp to next snippet field on Enter key.
imap <expr><CR> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<CR>"
"}}}

" Languages {{{
" ---------

" Python {{{
" ^^^^^^
	NeoBundleLazy 'klen/python-mode', '20e14aa3522', {'autoload': {'filetypes': ['python']}}
" Default keymappings are listed to put all the things in one place:
"
" [[      Jump to previous class or function (normal, visual, operator modes)
" ]]      Jump to next class or function  (normal, visual, operator modes)
" [M      Jump to previous class or method (normal, visual, operator modes)
" ]M      Jump to next class or method (normal, visual, operator modes)
" aC      Select a class. Ex: vaC, daC, yaC, caC (normal, operator modes)
" iC      Select inner class. Ex: viC, diC, yiC, ciC (normal, operator modes)
" aM      Select a function or method. Ex: vaM, daM, yaM, caM (normal, operator modes)
" iM      Select inner function or method. Ex: viM, diM, yiM, ciM (normal, operator modes)
"
	" <leader>b - the mnemonic is 'buffer'. One can treat this as a 'buffer
	" breakpoint' =)
    let g:pymode_breakpoint_bind = '<leader>bb'
    let g:pymode_syntax_highlight_equal_operator = 0
    let g:pymode_lint_checkers = ['pylint', 'pep8', 'pep257', 'pyflakes', 'mccabe']
    let g:pymode_lint_sort = ['E', 'C', 'I']
	" C0301 Line too long [pylint]
	" C0110 All modules should have docstrings [pep257]
	" E501 line too long [pep8]
    let g:pymode_lint_ignore = 'C0111,C0301,C0110,E501'
    " let g:pymode_lint = 0
	" Remember! If true, lint_unmodified *overrides* lint_on_write.
    let g:pymode_lint_on_write = 0
    " let g:pymode_lint_unmodified = 1
    " let g:pymode_debug = 1
    let g:pymode_rope_lookup_project = 0
	let g:pymode_rope_completion = 0
    let g:pymode_completion_provider = 'jedi'

    let g:pymode_run_bind = '<C-c>r'
    " <leader>l - the mnemonic is 'lint'
    nnoremap <leader>l :PymodeLint<cr>
    " <leader>r - the mnemonic is 'rope'
    let g:pymode_rope_goto_definition_bind = '<leader>rg'
    let g:pymode_rope_rename_bind = '<leader>rr'
    let g:pymode_rope_rename_module_bind = '<leader>r1r'
    let g:pymode_rope_module_to_package_bind = '<leader>r1p'
    let g:pymode_rope_organize_imports_bind = '<leader>ro'
    let g:pymode_rope_autoimport_bind = '<leader>ra'
" }}}

" HTML/CSS {{{
" ^^^^^^^^
    " Highlights the matching HTML tag when the cursor
    " is positioned on a tag.
    NeoBundleLazy 'gregsexton/MatchTag', {'autoload':{'filetypes':['html','xml']}}

    " HTML5 + inline SVG omnicomplete funtion, indent and syntax for Vim.
    NeoBundleLazy 'othree/html5.vim', {'autoload': {'filetypes': ['html', 'xhtml', 'css']}}

    " Colorizing color-literals with their values
    NeoBundleLazy 'ap/vim-css-color', {'autoload':{'filetypes':['css','scss','sass','less','styl']}}
" }}}

" Javascript {{{
" ^^^^^^^^^^
" One plugin per feature, huh?

    " Smart indent for javascript
    " http://www.vim.org/scripts/script.php?script_id=3081
    NeoBundleLazy 'lukaszb/vim-web-indent', {'autoload': {'filetypes': ['javascript']}}

    " Improve javascript syntax higlighting, needed for good folding,
    " and good-looking javascript code
    NeoBundleLazy 'jelera/vim-javascript-syntax', {'autoload': {'filetypes': ['javascript']}}

    " One more javascript plugin to compare in future
    " NeoBundle 'pangloss/vim-javascript'

	NeoBundleLazy 'kchmck/vim-coffee-script', {'autoload': {
		\ 'commands': ['CoffeeCompile', 'CoffeeLint', 'CoffeeMake', 'CoffeeRun', 'CoffeeWatch'],
		\ 'filetypes': ['coffee']
	\ }}
" }}}

" reST in Vim
NeoBundleLazy 'Rykka/riv.vim', {'autoload': {'filetypes': ['rst']}}

" }}}

" Plugin for changing cursor when entering in insert mode
" looks like it works fine with iTerm Konsole adn xerm
" Note that it doesn't work with the default ubuntu Terminal
NeoBundle 'jszakmeister/vim-togglecursor'

" Allow autoclose paired characters like [,] or (,),
" and add smart cursor positioning inside it,
NeoBundle 'Raimondi/delimitMate'
" Delimitmate place cursor correctly on multiline objects e.g.
" if you press enter in {} cursor still be
" in the middle line instead of the last
let delimitMate_expand_cr = 1
" Delimitmate place cursor correctly in singleline pairs e.g.
" if | denotes cursor position then
"   if you press space in {|} result will be { | } instead of { |}
let delimitMate_expand_space = 1

" Fix-up dot command behavior
" it's kind of service plugin
NeoBundle 'tpope/vim-repeat'


" For cool motions via <leader><leader>{{ motion }}, e.g. ,,w
NeoBundle 'Lokaltog/vim-easymotion'

" Nerdtree {{{
" --------
" A tree explorer plugin for vim.
NeoBundle 'scrooloose/nerdtree'

let NERDTreeWinSize = 30

" files/dirs to ignore in NERDTree (mostly the same as my svn ignores)
let NERDTreeIgnore=['\~$', '\.AppleDouble$', '\.beam$', 'build$',
\'dist$', '\.DS_Store$', '\.egg$', '\.egg-info$', '\.la$',
\'\.lo$', '\.\~lock.*#$', '\.mo$', '\.o$', '\.pt.cache$',
\'\.pyc$', '\.pyo$', '__pycache__$', '\.Python$', '\..*.rej$',
\'\.rej$', '\.ropeproject$', '\.svn$', '\.tags$' ]

" Disable bookmarks label, and hint about '?'
let NERDTreeMinimalUI=1

" <leader>t - the mnemonic is 'tree'
nnoremap <silent> <leader>tt :NERDTreeToggle<CR>
" Display current file in the NERDTree on the left
nnoremap <silent> <leader>tf :NERDTreeFind<CR>
" }}}

" Vim plugin that displays tags in a window, ordered by class etc.
NeoBundle "majutsushi/tagbar"

let g:tagbar_width = 30
let g:tagbar_foldlevel = 1
let g:tagbar_type_rst = {
	\ 'ctagstype': 'rst',
	\ 'kinds': [ 'r:references', 'h:headers' ],
	\ 'sort': 0,
	\ 'sro': '..',
	\ 'kind2scope': { 'h': 'header' },
	\ 'scope2kind': { 'header': 'h' }
\ }

" Toggle tagbar
nnoremap <silent> <F3> :TagbarToggle<CR>

" Vim plugin for intensely orgasmic commenting
NeoBundle 'scrooloose/nerdcommenter'
let NERDSpaceDelims = 1

" Syntastic {{{
"----------
" For syntax checking
NeoBundle 'scrooloose/syntastic'
" Enable autochecks
let g:syntastic_check_on_open=1
let g:syntastic_enable_signs=1

" For correct work of next/previous error navigation
let g:syntastic_always_populate_loc_list = 1

" Disable syntastic for python (managed by python-mode)
let g:syntastic_mode_map = {
	\ 'mode': 'active'
	\, 'active_filetypes': []
	\, 'passive_filetypes': ['python']
\ }
" }}}

" quoting/parenthesizing made simple
NeoBundle 'tpope/vim-surround'

" vim-airline {{{
" -----------
" Just beautiful statusbar
NeoBundle "bling/vim-airline"

let g:airline_detect_iminsert = 1
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_theme = 'wombat'
" Put here if I somewhen decide to do so
" Don't display encoding
" let g:airline_section_y = ''
" Don't display filetype
" let g:airline_section_x = ''
" }}}

" Color schemes
" -------------
NeoBundle 'flazz/vim-colorschemes'

" Unite
" -----
" As folks say, must replace CtrlP, FuzzyFinder, ack, yankring, LustyJuggler, buffer explorer
NeoBundle "Shougo/unite.vim"
NeoBundleLazy 'Shougo/unite-session', {'autoload':{'unite_sources':'session', 'commands': ['UniteSessionSave', 'UniteSessionLoad']}}
NeoBundleLazy 'Shougo/unite-outline', {'autoload':{'unite_sources':'outline'}}
NeoBundleLazy 'Shougo/unite-session', {'autoload':{'unite_sources':'session', 'commands': ['UniteSessionSave', 'UniteSessionLoad']}}
NeoBundleLazy 'osyo-manga/unite-quickfix', {'autoload':{'unite_sources': ['quickfix', 'location_list']}}
" Most recent files source for unite
NeoBundleLazy 'Shougo/neomru.vim', {'autoload': {'unite_sources': ['file_mru', 'directory_mru']}}
NeoBundleLazy 'thinca/vim-unite-history', { 'autoload' : { 'unite_sources' : ['history/command', 'history/search']}}
NeoBundleLazy 'ujihisa/unite-colorscheme', {'autoload':{'unite_sources': 'colorscheme'}}
NeoBundleLazy 'tsukkee/unite-help', {'autoload':{'unite_sources':'help'}}

let g:unite_source_session_path = g:SESSION_DIR
" Enable history for yanks
let g:unite_source_history_yank_enable = 1
" Make samll limit for yank history,
let g:unite_source_history_yank_limit = 40
let g:unite_source_menu_menus = {}
let g:unite_force_overwrite_statusline = 0
let g:unite_prompt = '>>> '
let g:unite_marked_icon = '✓'
let g:unite_update_time = 200
" Display unite on the bottom (or bottom right) part of the screen
let g:unite_split_rule = 'botright'
let g:unite_source_buffer_time_format = '(%d-%m-%Y %H:%M:%S) '
let g:unite_source_file_mru_time_format = '(%d-%m-%Y %H:%M:%S) '
let g:unite_source_directory_mru_time_format = '(%d-%m-%Y %H:%M:%S) '
" Set short limit for max most recent files count.
" It less unrelative recent files this way
let g:unite_source_file_mru_limit = 100
if executable('ag')
    let g:unite_source_grep_command='ag'
    let g:unite_source_grep_default_opts='--nocolor --nogroup -a -S'
    let g:unite_source_grep_recursive_opt=''
    let g:unite_source_grep_search_word_highlight = 1
elseif executable('ack')
    let g:unite_source_grep_command='ack'
    let g:unite_source_grep_default_opts='--no-group --no-color'
    let g:unite_source_grep_recursive_opt=''
    let g:unite_source_grep_search_word_highlight = 1
endif


nnoremap [menu] <Nop>
nmap <LocalLeader> [menu]
nnoremap <silent>[menu]u :Unite -silent -winheight=12 menu<CR>
nmap <LocalLeader>c <Plug>(unite_exit)

" Buffers, tabs and windows operations {{{

" <leader>u - the mnemonic is 'unite'
nnoremap <silent> <leader>ub :<C-u>Unite buffer<CR>

let g:unite_source_menu_menus.navigation = {
	\ 'description' : '     navigate by buffers, tabs & windows                   ⌘ [space]b', }

let g:unite_source_menu_menus.navigation.command_candidates = [
	\['buffers                                                    ⌘ ,ub', 'Unite buffer'],
	\['tabs', 'Unite tab'],
	\['windows', 'Unite window'],
	\['location list', 'Unite location_list'],
	\['quickfix', 'Unite quickfix'],
	\['new vertical window                                        ⌘ ,v', 'vsplit'],
	\['new horizontal window                                      ⌘ ,h', 'split'],
	\['close current window                                       ⌘ ,k', 'close'],
	\['delete buffer                                              ⌘ ,K', 'bd'],
\]

exe 'nnoremap <silent>[menu]b :Unite -silent -winheight='.(len(g:unite_source_menu_menus.navigation.command_candidates) + 2).' menu:navigation<CR>'

" }}}

" File's operations {{{
nnoremap <leader>uo :<C-u>Unite -no-split -start-insert file<CR>
nnoremap <leader>uO :<C-u>Unite -no-split -start-insert file_rec/async:!<CR>
nnoremap <leader>um :<C-u>Unite -no-split file_mru<CR>

let g:unite_source_menu_menus.files = {
	\ 'description' : '          files & dirs                                          ⌘ [space]o',}

let g:unite_source_menu_menus.files.command_candidates = [
	\['open file                                                  ⌘ ,uo', 'normal ,uo'],
	\['open file with recursive search                            ⌘ ,uO', 'normal ,uO'],
	\['open more recently used files                              ⌘ ,um', 'normal ,um'],
	\['edit new file', 'Unite file/new'],
	\['search directory', 'Unite directory'],
	\['search recently used directories', 'Unite directory_mru'],
	\['search directory with recursive search', 'Unite directory_rec/async'],
	\['make new directory', 'Unite directory/new'],
	\['change working directory', 'Unite -default-action=lcd directory'],
	\['know current working directory', 'Unite -winheight=3 output:pwd'],
	\['quick save                                                 ⌘ ,ww', 'normal ,ww'],
\]

exe 'nnoremap <silent>[menu]o :Unite -silent -winheight='.(len(g:unite_source_menu_menus.files.command_candidates) + 2).' menu:files<CR>'
" }}}

" Search files {{{
nnoremap <silent><Leader>ua :Unite -silent -no-quit grep<CR>

let g:unite_source_menu_menus.grep = {
	\ 'description' : '           search files                                          ⌘ [space]a',}

let g:unite_source_menu_menus.grep.command_candidates = [
	\['grep (ag → ack → grep)                                     ⌘ ,ua', 'Unite -no-quit grep'],
	\['find', 'Unite find'],
	\['vimgrep (very slow)', 'Unite vimgrep'],
\]

exe 'nnoremap <silent>[menu]a :Unite -silent -winheight='.(len(g:unite_source_menu_menus.grep.command_candidates) + 2).' menu:grep<CR>'
" }}}

" Yanks, registers & history {{{
nnoremap <silent><Leader>ui :Unite -silent history/yank<CR>
nnoremap <silent><Leader>ur :Unite -silent register<CR>

let g:unite_source_menu_menus.registers = {
	\ 'description' : '      yanks, registers & history                            ⌘ [space]i'}

let g:unite_source_menu_menus.registers.command_candidates = [
	\['yanks                                                      ⌘ ,ui', 'Unite history/yank'],
	\['commands       (history)                                   ⌘ q:', 'Unite history/command'],
	\['searches       (history)                                   ⌘ q/', 'Unite history/search'],
	\['registers                                                  ⌘ ,ur', 'Unite register'],
	\['messages', 'Unite output:messages'],
\]

exe 'nnoremap <silent>[menu]i :Unite -silent -winheight='.(len(g:unite_source_menu_menus.registers.command_candidates) + 2).' menu:registers<CR>'
" }}}

" Sessions {{{
let g:unite_source_menu_menus.sessions = {
	\ 'description' : '       sessions                                              ⌘ [space]s'}

let g:unite_source_menu_menus.sessions.command_candidates = [
	\['session list', 'Unite session'],
	\['load last auto-session', 'UniteSessionLoad last.vim'],
	\['save session (default)', 'UniteSessionSave'],
	\['save session (custom)', 'exe "UniteSessionSave " input("name: ")'],
\]

exe 'nnoremap <silent>[menu]s :Unite -silent -winheight='.(len(g:unite_source_menu_menus.sessions.command_candidates) + 2).' menu:sessions<CR>'
" }}}

" Vim {{{
let g:unite_source_menu_menus.vim = {
	\ 'description' : '            vim                                                   ⌘ [space]v'}

let g:unite_source_menu_menus.vim.command_candidates = [
	\['choose colorscheme', 'Unite colorscheme -auto-preview'],
	\['mappings', 'Unite mapping -start-insert'],
	\['edit configuration file (vimrc)                            ⌘ ,ev', 'edit $MYVIMRC'],
	\['vim help', 'Unite -start-insert help'],
	\['vim commands', 'Unite -start-insert command'],
	\['vim functions', 'Unite -start-insert function'],
	\['vim runtimepath', 'Unite -start-insert runtimepath'],
	\['vim command output', 'Unite output'],
	\['unite sources', 'Unite source'],
	\['launch executable (dmenu like)', 'Unite -start-insert launcher'],
\]

exe 'nnoremap <silent>[menu]v :Unite -silent -winheight='.(len(g:unite_source_menu_menus.vim.command_candidates) + 2).' menu:vim<CR>'
" }}}
"
" Text edition {{{
let g:unite_source_menu_menus.text = {
	\ 'description' : '           text edition                                          ⌘ [space]e'}

let g:unite_source_menu_menus.text.command_candidates = [
	\['toggle search results highlight                            ⌘ ,[space]', 'set invhlsearch'],
	\['toggle line numbers                                        ⌘ ,on', 'call ToggleRelativeAbsoluteNumber()'],
	\['toggle wrapping                                            ⌘ ,ow', 'normal ,ow'],
	\['show hidden chars                                          ⌘ ,ol', 'normal ,ol!'],
	\['toggle fold                                                ⌘ /', 'normal za'],
	\['open all folds                                             ⌘ zR', 'normal zR'],
	\['close all folds                                            ⌘ zM', 'normal zM'],
	\['toggle paste mode                                          ⌘ ,p', 'normal ,p'],
	\['show current char info                                     ⌘ ga', 'normal ga'],
\]

exe 'nnoremap <silent>[menu]e :Unite -silent -winheight='.(len(g:unite_source_menu_menus.text.command_candidates) + 2).' menu:text<CR>'
" }}}

" Neobundle {{{
let g:unite_source_menu_menus.neobundle = {
	\ 'description' : '      plugins administration with neobundle                 ⌘ [space]n'}

let g:unite_source_menu_menus.neobundle.command_candidates = [
	\['neobundle', 'Unite neobundle'],
	\['neobundle log', 'Unite neobundle/log'],
	\['neobundle lazy', 'Unite neobundle/lazy'],
	\['neobundle update', 'Unite neobundle/update'],
	\['neobundle search', 'Unite neobundle/search'],
	\['neobundle install', 'Unite neobundle/install'],
	\['neobundle check', 'Unite -no-empty output:NeoBundleCheck'],
	\['neobundle docs', 'Unite output:NeoBundleDocs'],
	\['neobundle clean', 'NeoBundleClean'],
	\['neobundle list', 'Unite output:NeoBundleList'],
	\['neobundle direct edit', 'NeoBundleDirectEdit'],
\]

exe 'nnoremap <silent>[menu]n :Unite -silent -winheight='.(len(g:unite_source_menu_menus.neobundle.command_candidates) + 2).' menu:neobundle<CR>'
" }}}

" Tags
nnoremap <silent><Leader>ut :Unite -silent -vertical -winwidth=40
    \ -direction=topleft -toggle outline<CR>

call neobundle#end()

syntax on "Включить подсветку синтаксиса


call unite#filters#matcher_default#use(['matcher_fuzzy'])
call unite#filters#sorter_default#use(['sorter_rank'])


colorscheme zenburn " Цветовая схема

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

" Local settings
" ================
if filereadable($HOME . "/.vim_local")
    source $HOME/.vim_local
endif


" Project settings
" ================

" enables the reading of .vimrc, .exrc and .gvimrc in the current directory.
set exrc

" must be written at the last. see :help 'secure'.
set secure
